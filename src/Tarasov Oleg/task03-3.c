#include <stdio.h>
#include <locale.h>
#include <stdlib.h>

#define MAX_LENGTH 100

void InsertionSort(int** arr, int num)
{
	int key[2], i;
	for (int j = 1; j < num; j++) {
		key[0] = arr[j][0];
		key[1] = arr[j][1];
		i = j - 1;
		while (i >= 0 && arr[i][0] > key[0]) {
			arr[i + 1][0] = arr[i][0];
			arr[i + 1][1] = arr[i][1];
			i--;
		}
		arr[i + 1][0] = key[0];
		arr[i + 1][1] = key[1];
	}
}

void WriteStringsInLengthOrder(char** arr, int** strLength, int num)
{
	InsertionSort(strLength, num);
	printf("������ � ������� ����������� �� �����:\n");
	for (int i = 0; i < num; i++) {
		for (int j = 0; j < MAX_LENGTH && arr[strLength[i][1]][j] != 0; j++) {
			printf("%c", arr[strLength[i][1]][j]);
		}
		printf("\n");
	}
}

int main()
{
	setlocale(LC_ALL, "Russian");
	char **arr;
	int ** strLength;
	int num = 0;
	arr = (char**)calloc(num, sizeof(char*));
	strLength = (int**)calloc(num, sizeof(int*));

	printf("������� ������. ����� ��������� ����� ������� ENTER.\n");

	while (true) {
		char str[MAX_LENGTH];
		fgets(str, MAX_LENGTH, stdin);
		if (str[0] == '\n' || str[0] == '\0')
			break;
		num++;
		arr = (char**)realloc(arr, num*sizeof(char*));
		strLength = (int**)realloc(strLength, num*sizeof(int*));

		arr[num - 1] = (char*)calloc(MAX_LENGTH, sizeof(char));
		strLength[num - 1] = (int*)calloc(2, sizeof(int));	

		for (int i = 0; i < MAX_LENGTH; i++) {
			if (str[i] == '\n' || str[i] == '\0') break;
			arr[num - 1][i] = str[i];
			strLength[num - 1][0] ++;
		}
		strLength[num - 1][1] = num - 1;
	}

	WriteStringsInLengthOrder(arr, strLength, num);

	for (int i = 0; i < num; i++) {
		free(arr[i]);
		free(strLength[i]);
	}
	free(arr);
	free(strLength);
	return 0;
}