#include <stdio.h>
#include <locale.h>
#include <stdlib.h>

#define MAX_LENGTH 100

void WriteStringsInBackOrder(char** arr, int num) 
{
	for(int i = num-1; i >= 0; i--) {
		for(int j = 0; j < MAX_LENGTH && arr[i][j] != 0; j++) {
			printf("%c", arr[i][j]);
		}
		printf("\n");
	}
}

int main()
{
	setlocale(LC_ALL, "Russian");
	char **arr;
	int num = 0;
	arr = (char**)calloc(num, sizeof(char*));

	printf("������� ������. ����� ��������� ����� ������� ENTER.\n");

	while(true) {
		char str[MAX_LENGTH];
		fgets(str, MAX_LENGTH, stdin);
		if(str[0] == '\n' || str[0] == '\0')
			break;
		num++;
		arr = (char**)realloc(arr, num*sizeof(char*));
		arr[num-1] = (char*)calloc(MAX_LENGTH, sizeof(char));

		for(int i = 0; i < MAX_LENGTH; i++) {
			if(str[i] == '\n' || str[i] == '\0') break;
			arr[num-1][i] = str[i];
		}
	}

	WriteStringsInBackOrder(arr, num);

	for(int i = 0; i < num; i++) 
		free(arr[i]);
	free(arr);
	return 0;
}