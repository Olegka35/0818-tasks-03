#include <stdio.h>
#include <locale.h>
#include <stdlib.h>
#include <time.h>

#define MAX_LENGTH 100

void WriteStringsInRandomOrder(char** arr, int num)
{
	srand(time(0));
	int i, random, temp;
	int * tempArr = (int*)malloc(num * sizeof(int));
	for (i = 0; i < num; i++)
		tempArr[i] = i;
	for (i = num; i > 0; i--) {
		random = rand() % i;
		temp = tempArr[random];
		tempArr[random] = tempArr[i - 1];
		tempArr[i - 1] = temp;
	}
	printf("��������� ������ � ��������� �������:\n");
	for (i = 0; i < num; i++) {
		for (int j = 0; j < MAX_LENGTH && arr[tempArr[i]][j] != 0; j++)
			printf("%c", arr[tempArr[i]][j]);
		printf("\n");
	}
}

int main()
{
	setlocale(LC_ALL, "Russian");
	char **arr;
	int num = 0;
	arr = (char**)calloc(num, sizeof(char*));

	printf("������� ������. ����� ��������� ����� ������� ENTER.\n");
	while (true) {
		char str[MAX_LENGTH];
		fgets(str, MAX_LENGTH, stdin);
		if (str[0] == '\n' || str[0] == '\0')
			break;
		num++;
		arr = (char**)realloc(arr, num*sizeof(char*));
		arr[num - 1] = (char*)calloc(MAX_LENGTH, sizeof(char));
		for (int i = 0; i < MAX_LENGTH; i++) {
			if (str[i] == '\n' || str[i] == '\0') break;
			arr[num - 1][i] = str[i];
		}
	}
	WriteStringsInRandomOrder(arr, num);

	for (int i = 0; i < num; i++)
		free(arr[i]);
	free(arr);
	return 0;
}